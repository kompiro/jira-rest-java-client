package com.atlassian.jira.rest.client;

public interface Callback {
	void execute();
}
